import std.stdio;
import std.file;
import std.string;
import std.array;
import std.conv;
import std.net.curl;
import std.path;
import std.range;
import std.format;
import std.algorithm;
import std.xml;
import std.regex;
import std.getopt;
import std.datetime;
import std.parallelism;
import std.zip;
import std.regex;
import core.thread;

import colorize;
import dini;
static import dom = arsd.dom; //to prevent name collision with xml
import ddbc.all;


//DateTime currentdt; // see main. it's globally

static this()
{
	DateTime currentdt = cast(Date)(Clock.currTime()); // It's better to declarate globally
}

void main(string[] args)
{



	string appName = "-=GeoDataLoader App v.1.0.1=-\n".color(fg.green);
	cwriteln(appName);
	core.thread.Thread.sleep( dur!("seconds")(1) );
	
	static string fff = "sdfsfdf";

	//We need 2 instance for start: parseconfig and DBConnect, or App can't run
	//Then we will use them everywhere
	auto parseconfig = new ParseConfig();
	core.thread.Thread.sleep( dur!("seconds")(1) );
	auto db = new DBConnect(parseconfig);

	if (parseconfig.obsmpplots_load == "true")
	{
		auto obsmpplots = new ObsmpPlots(db);
		obsmpplots.getPlots();
	}

	if(parseconfig.nadisa_load == "true")
	{
		auto nadisa = new Nadisa(db);
		nadisa.getPlots();
	}
	

	core.thread.Thread.sleep( dur!("seconds")(1) );
	if(parseconfig.emsc_csem_load == "true")
		{
			if (checkLink(parseconfig.emsc_csem)) // if link in config is alive processing it
			{
				auto seismodownload = new SeismoDownload(parseconfig);
				seismodownload.parsecsem();
				db.EQInsert(seismodownload.eqs);
			}
			else 
			cwritefln("Look like link is dead: %s", parseconfig.emsc_csem.color(fg.red));
		}
	
		if(parseconfig.usgs_load == "true")
		{
			if (checkLink(parseconfig.usgs)) // if link in config is alive processing it
			{
				//auto db = new DBConnect(parseconfig); // already exists
				auto seismodownload = new SeismoDownload(parseconfig);
				seismodownload.parseusgs();
				db.EQInsert(seismodownload.eqs);
				//scope(exit) db.stmt.close(); //now we can close connection
			}
			else 
			cwritefln("Look like link is dead: %s", parseconfig.emsc_csem.color(fg.red));
		}

	if(parseconfig.kakiokajma_load == "true")
	{
		auto kakiokajma = new Kakiokajma(db);
		kakiokajma.kakiokajma();
	}

	if(parseconfig.solarindex_load == "true")
	{
		auto solarindex = new SolarIndex(parseconfig, db);
		solarindex.parse();
		core.thread.Thread.sleep( dur!("seconds")(1) );
	}

	if(parseconfig.dgdindex_load == "true")
	{
		if (parseconfig.dgd) // DO NOT work with FTP! old version: checkLink(parseconfig.dgd)
		{
			auto dgdindex = new DGDIndex(parseconfig, db);
			dgdindex.parse();
		}
		else 
			cwritefln("Look like link is dead: %s", parseconfig.dgd.color(fg.red));
	}

	if(parseconfig.ftp_load == "true")
	{
		auto ftplogs2db = new FTP(parseconfig, db);
		ftplogs2db.getLogsList(parseconfig.ftplocalpath);
	}

	if(parseconfig.ftp_shgm3_load == "true")
	{
		auto ftplogs2db = new FTP(parseconfig, db);
		ftplogs2db.getLogsList(parseconfig.ftplocalpath_shgm3);
	}


	//if one of the config sections is true, create instance of getimgfulllinks that include other small section
	// and instance of IMGDownload
	core.thread.Thread.sleep( dur!("msecs")(1000) );
	if (parseconfig.nrlmry_load == "true" || parseconfig.jmaimgs_load == "true" || parseconfig.cwbimgs_load == "true" || parseconfig.kalpana_load == "true")
	{
			auto getimgfulllinks = new GetImgFullLinks(parseconfig, db); //2 instance 

			if(parseconfig.nrlmry_load == "true")
			{
				// here is another URL with another way of loading
				writeln("Loading NRL sections to DB");
				core.thread.Thread.sleep( dur!("seconds")(1) );	
				// do not forget uncomment
				getimgfulllinks.nrlmrynavymil2();
			}

			if(parseconfig.jmaimgs_load == "true")
			{
				writeln("Loading JMA section to DB");
				core.thread.Thread.sleep( dur!("seconds")(1) );				
				getimgfulllinks.jmaimgs();
			}
			
			if(parseconfig.cwbimgs_load == "true")
			{
				writeln("Loading CWB section to DB");
				core.thread.Thread.sleep( dur!("seconds")(1) );					
				getimgfulllinks.cwbimgs();
			}
			
			if(parseconfig.kalpana_load == "true")
			{
				writeln("Loading KALPANA section to DB");
				core.thread.Thread.sleep( dur!("seconds")(1) );						
				getimgfulllinks.kalpana();
			}

	}

	else
		writeln("Images DO NOT load because all sections in config.ini are set to false");

	// loading IMGs to DB and DB2FS are separate sections
	if(parseconfig.imgs_db2fs == "true")
	{
		writeln("Loading images from DB to FS");
		core.thread.Thread.sleep(dur!("seconds")(1));
		auto imgdownload = new IMGDownload (db, parseconfig);
		imgdownload.imagedownload("SELECT src, name FROM test.imgs WHERE status is NULL;"); // download ALL
	}

	if(parseconfig.imgs_db2fs_for_specific_date == "true" || parseconfig.imgs_db2fs != "true") // we need check if not both set to true
	{
		writeln("Loading images from DB to FS for SPECIFIED DATE");
		core.thread.Thread.sleep(dur!("seconds")(1));
		auto imgdownload = new IMGDownload (db, parseconfig);
		// download only for current date. Date may have format YYYY-MM-DD or YYYYMMDD
		// here can be added user input, but now current day hardcoded
		// original request: SELECT * FROM test.imgs WHERE src LIKE CONCAT('%', REPLACE(CAST(CURDATE()as char), "-", ""), '%') OR CONCAT('%', CAST(CURDATE()as char), '%')
		string sql = "SELECT * FROM test.imgs WHERE src LIKE CONCAT('%', REPLACE(CAST(CURDATE()as char), \"-\", \"\"), '%') OR CONCAT('%', CAST(CURDATE()as char), '%')";
		imgdownload.imagedownload(sql);
	}


	scope(exit) db.stmt.close(); //now we can close connection
	cwritefln("[Complete!]".color(fg.yellow));
	core.thread.Thread.sleep( dur!("seconds")(20));

}


class ParseConfig
{
	string dbname;
	string dbuser;
	string dbpass;
	string dbhost;
	string dbport;
	string dbtablename_eq;
	string dataproj;
	string emsc_csem; 
	string usgs; 
	string dgd;
	/////////////IMGs////////////////////
	string [] pagewithimgforparsing; // array of list IMG urls for parsing from config
	string imgs_db2fs; // load images to FS or no
	string imgs_db2fs_for_specific_date;
	//we need to storage info about IMGs table
	string dbIMGsTableName;


	string solarindex; // solar and geomagnetic index
	////ftp////
	string ftplocalpath;
	string ftplocalpath_shgm3;

	string emsc_csem_load;
	string usgs_load;
	string solarindex_load;
	string ftp_load;
	string ftp_shgm3_load;
	//string geomagneticURL_load;
	string nrlmry_load;
	string jmaimgs_load;
	string cwbimgs_load;
	string kalpana_load;
	string dgdindex_load;


	//plots
	string kakiokajma_load;
	string obsmpplots_load;
	string nadisa_load;

	//for FTP
	string ftp_load_only_1_minuts;

this()
	{
		try
		{
			//getcwd do not return correct path if run from task shoulder
			string confpath = buildPath((thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]), "config.ini");
			//writefln(thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]); // get path without extention +1 is for getting last slash

			//string confpath = buildPath(thisExePath, "config.ini");
			if (!exists(confpath)) 
				{
					writeln("ERROR: config.ini do not exists");
				}
			auto config = Ini.Parse(confpath);
			try
			{
				this.dbname = config.getKey("dbname");
				this.dbuser = config.getKey("dbuser");
				this.dbpass = config.getKey("dbpass");
				this.dbhost = config.getKey("dbhost");
				this.dbport = config.getKey("dbport");
				this.dbtablename_eq = config.getKey("dbtablename_eq");
				this.dataproj = config.getKey("dataproj");
				this.emsc_csem = config.getKey("emsc_csem");
				this.usgs = config.getKey("usgs");
				this.dgd = config.getKey("dgd");
				//////////////////////////IMGs//////////////////////////////////
				//Now we collect img-urls-page in array. Next we will check them and extract FULL links to images
				//Links here DO NOT include FULL imgs urls!!! Just URLs for PAGEs for parsing!
				////////////////////////////////////////////////////////////////
				// "" - default value if base value could not be parse!
				this.dbIMGsTableName = config.getKey("dbIMGsTableName", "");
	
				this.solarindex = config.getKey("solarindex", "");

				this.ftplocalpath = config.getKey("ftplocalpath", "");
				this.ftplocalpath_shgm3 = config.getKey("ftplocalpath_shgm3", "");

				////// ON and OFF sectons
				this.emsc_csem_load = config.getKey("emsc_csem_load");
				this.usgs_load = config.getKey("usgs_load");
				this.solarindex_load = config.getKey("solarindex_load");
				this.ftp_load = config.getKey("ftp_load");
				this.ftp_shgm3_load = config.getKey("ftp_shgm3_load");
				//this.geomagneticURL_load = config.getKey("geomagneticURL_load");
				this.nrlmry_load = config.getKey("nrlmry_load");
				this.jmaimgs_load = config.getKey("jmaimgs_load");
				this.cwbimgs_load = config.getKey("cwbimgs_load");
				this.kalpana_load = config.getKey("kalpana_load");
				this.dgdindex_load = config.getKey("dgdindex_load");

				this.kakiokajma_load = config.getKey("kakiokajma_load");
				this.obsmpplots_load = config.getKey("obsmpplots_load");
				this.nadisa_load = config.getKey("nadisa_load");
				this.ftp_load_only_1_minuts = config.getKey("ftp_load_only_1_minuts");
				this.imgs_db2fs = config.getKey("imgs_db2fs");
				this.imgs_db2fs_for_specific_date = config.getKey("imgs_db2fs_for_specific_date"); // future that allow load only date for specified date

			}
			catch (Exception msg)
			{
				cwritefln("ERROR: Can't parse config: %s".color(fg.red), msg.msg);
			}		
		}
		catch(Exception msg)
		{
			cwriteln(msg.msg.color(fg.red));
			core.thread.Thread.sleep( dur!("msecs")(1000));
		}	
	}


}


struct EQ
{
	string region; //title
	string lat;
	string lon;
	string depth;
	string magnitude;
	string time;
}

class SeismoDownload
{
	//string emsc_csem;
	//string usgs;
	ParseConfig parseconfig;

	string pagecontent;
	
	EQ[] eqs;
	EQ eq;

	this(ParseConfig parseconfig)
	{
		this.parseconfig = parseconfig;
	}
	
	void parsecsem()
	{
	
		try
		{
			writeln("[Earth Quakes]");
			pagecontent = get(parseconfig.emsc_csem).idup;
			//writeln(pagecontent);

			check(pagecontent);
			auto pageDOM = new DocumentParser(pagecontent);
			pageDOM.onStartTag["item"] = (ElementParser pageDOM)
			{
				

				pageDOM.onEndTag["title"] = (in Element e) { eq.region = e.text(); }; //region
				pageDOM.onEndTag["geo:lat"] = (in Element e) { eq.lat = e.text(); }; //geo:lat
				pageDOM.onEndTag["geo:long"] = (in Element e) { eq.lon = e.text(); }; //geo:lon
				pageDOM.onEndTag["emsc:depth"] = (in Element e) { eq.depth = e.text(); };
				pageDOM.onEndTag["emsc:magnitude"] = (in Element e) { eq.magnitude = e.text(); };
				pageDOM.onEndTag["emsc:time"] = (in Element e) { eq.time = e.text(); };

				pageDOM.parse();

				//Filtering Data. Removing crap data, Removing whitespaces.
				// WARNING: some data may include apostrophe like O'Chily we need to remove it!!!  
				eq.region = strip(eq.region[8..$].replace("'", " "));
				eq.lat = strip(eq.lat);
				eq.lon = strip(eq.lon);
				eq.depth = strip(chomp(eq.depth, "f"));
				eq.magnitude = strip(eq.magnitude[2..$]);
				eq.time = strip(chomp(eq.time, "UTC"));

				eqs ~= eq;

			};
		
			pageDOM.parse();	

		}
		
		catch (Exception msg)
		{
			cwriteln("ERROR: parsecsem section".color(fg.red));
		}


	}
	
		void parseusgs()
		{
			try
			{
				//string url = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_day.csv";
				string url = parseconfig.usgs;
				
				foreach(line; url.byLine())
				{
					if (to!string((line[0]))== "t") continue; // stupid way to check header
					//writeln(line);
					//readln();
					eq.time = ((to!string(line)).split(",")[0]).replace("T", " ").replace("Z", "");
					eq.lat = (to!string(line)).split(",")[1];
					eq.lon = (to!string(line)).split(",")[2];
					eq.depth = (to!string(line)).split(",")[3];
					eq.magnitude = (to!string(line)).split(",")[4];
					eq.region = (to!string(line)).split(",")[13];

					eqs ~= eq;
				}
			}

			catch (Exception msg)
			{
				cwriteln("ERROR: parseusgs section".color(fg.red));
			}
		}


}

class DBConnect
{
	Statement stmt;
	ParseConfig parseconfig;

	this(ParseConfig parseconfig)
	{
		try
			{
			    this.parseconfig = parseconfig;
			    MySQLDriver driver = new MySQLDriver();
			    string url = MySQLDriver.generateUrl(parseconfig.dbhost, to!short(parseconfig.dbport), parseconfig.dbname);
			    string[string] params = MySQLDriver.setUserAndPassword(parseconfig.dbuser, parseconfig.dbpass);

				DataSource ds = new ConnectionPoolDataSourceImpl(driver, url, params);

				auto conn = ds.getConnection();
				scope(exit) conn.close();

				stmt = conn.createStatement();
				writefln("\n[Database connection OK]");
			}
		catch (Exception ex)
		{
			writefln(ex.msg);
			writeln("Could not connect to DB. Please check settings");
		}

	}	

	
	 void EQInsert(EQ[] eqs) // getting EQ data and pasting them. Because instance is already exists!
	 	{		

	 	 //EQ[] eqs;	
			try 
				{
					string eq_before;
					auto request_before = stmt.executeQuery("select COUNT(*) from " ~ parseconfig.dbname ~ "." ~ parseconfig.dbtablename_eq);
					while(request_before.next())
							eq_before = request_before.getString(1);

					foreach(eq; eqs)
					{
						//simple insert
						//string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbtablename_eq ~ "(DateTime, Points, Lat, Lon, Magnitude, Depth, Region) VALUES (" "'"~ eq.time ~ "',"  ~ "GeomFromText('POINT(" ~ eq.lat ~ " " ~ eq.lon ~ ")'," ~ parseconfig.dataproj ~ ")," ~ eq.lat ~ ", " ~ eq.lon ~ ", " ~ eq.magnitude ~ ", " ~ eq.depth ~ ", " ~ "'" ~ eq.region ~ "');");
						
						// insert and update
						string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbtablename_eq ~ "(DateTime, Lat, Lon, Magnitude, Depth, Region) VALUES (" "'"~ eq.time ~ "',"  ~ eq.lat ~ ", " ~ eq.lon ~ ", " ~ eq.magnitude ~ ", " ~ eq.depth ~ ", " ~ "'\"" ~ eq.region ~ "\"') " ~ " ON DUPLICATE KEY UPDATE Lat= " ~ eq.lat ~ ", Lon=" ~ eq.lon ~ ", Magnitude=" ~ eq.magnitude ~ ", Depth=" ~ eq.depth ~ ", Region='" ~ eq.region ~ "';").replace("\"\"","\"");
						//writeln(sqlinsert);
					 	auto rs = stmt.executeUpdate(sqlinsert);

					 }
					
					string eq_after;
					auto request_after = stmt.executeQuery("select COUNT(*) from test.eq;");
					while(request_after.next())
						eq_after = request_after.getString(1);

					auto delta = (to!int(eq_after)) - (to!int(eq_before));
					cwritefln("Total in DB: %s | New added: %s\n".color(fg.yellow), eq_before, delta);
					
				}

			catch (Exception ex)
				{
						cwriteln("Can't INSERT data to the table!".color(fg.red));
						writeln(ex.msg);
						exit(0);
			 	}
		}

		void GeomagneticInsert(string [] sqlinserts) 
		{
			try
			{
				string geomagnetic_before;
				auto request_before = stmt.executeQuery("select COUNT(*) from " ~ parseconfig.dbname ~ ".geomagnetic");
				while(request_before.next())
				geomagnetic_before = request_before.getString(1); // one = data 
				foreach(sql;sqlinserts)
				{
					auto rs = stmt.executeUpdate(sql);
			
				}

				string geomagnetic_after;
				auto request_after = stmt.executeQuery("select COUNT(*) from " ~ parseconfig.dbname ~ ".geomagnetic");
				while(request_after.next())
					geomagnetic_after = request_after.getString(1);

				auto delta = (to!int(geomagnetic_after)) - (to!int(geomagnetic_before));

				cwritefln("Total in DB: %s | New added: %s\n".color(fg.yellow), geomagnetic_before, delta);
			}

			catch(Exception e)
			{
				writeln("Error in GeomagneticInsert section");
				writeln(e.msg);
			}

		}

		void IMGsInsert(string [] fullimgurl, string [] imgnames)
		{
			try
			{
				writeln("IMGsInsert in DB function start...\n");

				int len = fullimgurl.length;
				int step = to!int(len/75);
			//	writefln("step = %s, len=%s", step, len);

				//statistic part
				string img_before;
				try {
						auto request_before = stmt.executeQuery("select COUNT(*) from " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName);
						
						while(request_before.next())
						img_before = request_before.getString(1); // one = data 
					}
				catch (Exception ex)
					{
						writeln(ex.msg);
						writeln("Some error when SELECT COUNT(*) occur");
					}

				auto starttime1 = Clock.currTime(UTC());
				foreach (i, url; fullimgurl)
				{
					//working string! DST is dropped!
					//string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName ~ "(src, dst) VALUES (" "'"~ url ~ "', "~ "'"~ imgnames[i] ~ "')" );
					string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName ~ "(src, name) VALUES (" "'"~ url ~ "', "~ "'"~ imgnames[i] ~ "')" ~ " ON DUPLICATE KEY UPDATE src =""'" ~ url ~ "'");
					
					// WORK! Do not forget that name or other field should have UNIQUE index 
					//string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName ~ "(src, name) VALUES (" "'"~ url ~ "', "~ "'"~ imgnames[i] ~ "')" ~ " ON DUPLICATE KEY UPDATE src =""'" ~ url ~ "'");
					//	writeln(sqlinsert);
					//	writeln();
					auto rs = stmt.executeUpdate(sqlinsert);
				}

				string img_after;
				auto request_after = stmt.executeQuery("select COUNT(*) from " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName);
				while(request_after.next())
					img_after = request_after.getString(1);

				auto delta = (to!int(img_after)) - (to!int(img_before));

				auto resulttime1 = Clock.currTime(UTC()) - starttime1;

				cwritefln("Total in DB: %s | New added: %s".color(fg.yellow), img_before, delta);
				cwritefln("Insert time (ON DUPLICATE UPDATE): %s".color(fg.yellow), resulttime1);
			}

			catch(Exception e)
			{
				writeln("Error in IMGsInsert");
				writeln(e.msg);
			}

/*
//test2 part commented

		/////////// TEST SPEED OF SELECT AND THEN INSER ////////////////////
			auto starttime2 = Clock.currTime(UTC());		
			foreach(i, url; fullimgurl)
			{
				auto test = stmt.executeQuery("SELECT * FROM test.imgs WHERE name='" ~ imgnames[i] ~ "'");
			//	auto test = stmt.executeQuery("SELECT * FROM test.imgs WHERE name= '20141227.0701.goes_15.visir.bckgr.NE_Pacific_Overview.NGT.jpg'");
				
				if(test.next() == false)
				{
					string sqlinsert = ("INSERT INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName ~ "(src, dst) VALUES (" "'"~ url ~ "', "~ "'"~ imgnames[i] ~ "')" );
					auto rs = stmt.executeUpdate(sqlinsert);
				}
				else
					writeln("yes");				
			}
			auto resulttime2 = Clock.currTime(UTC()) - starttime2;


		/////////// INSERT IGNORE ////////////////////

// test 3 part INSERT IGNORE
			auto starttime3 = Clock.currTime(UTC());		
			foreach(i, url; fullimgurl)
			{

				string sqlinsert = ("INSERT IGNORE INTO " ~ parseconfig.dbname ~ "." ~ parseconfig.dbIMGsTableName ~ "(src, dst) VALUES (" "'"~ url ~ "', "~ "'"~ imgnames[i] ~ "')" );
				auto rs = stmt.executeUpdate(sqlinsert);
			}
			auto resulttime3 = Clock.currTime(UTC()) - starttime3;

			writeln("\nInsert time (ON DUPLICATE UPDATE): ", resulttime1); 
			writeln("\nInsert time (SELECT and if NULL INSERT)): ", resulttime2);
			writeln("\nInsert time (INSERT IGNORE)): ", resulttime3);
*/

		}


	void solarindexinsert(string [] sqlinsert)
	{
		try
		{
			foreach (sql; sqlinsert)
			{
				auto rs = stmt.executeUpdate(sql);
			}
		}

		catch(Exception e)
		{
			writeln("Error in during pasting data to SQL in section solarindexinsert");
			writeln(e.msg);
		}
	}


	void dgdindexinsert(string [] sqlinsert)
	{
		try
		{
			writeln("dgd (part of HTMP) index");
			int count_before;
			int count_after;
			auto total_before = stmt.executeQuery("select COUNT(*) from test.dgd;");
			while(total_before.next())
			count_before = to!int(total_before.getString(1));	

			foreach (sql; sqlinsert)
			{
				auto rs = stmt.executeUpdate(sql);
			}

			auto total_after = stmt.executeQuery("select COUNT(*) from test.dgd;");
			while(total_after.next())
			count_after = to!int(total_after.getString(1));	

			int delta = (to!int(count_after)) - (to!int(count_before));
			cwritefln("Total in DB: %s | New added: %s".color(fg.yellow), count_before, delta);
		}

		catch(Exception e)
		{
			writeln("Error in dgdindexinsert");
			writeln(e.msg);
		}
	}


	void getFTPLogContent(string [] logfullname)
	{
		// TODO: add intersection of zip and logs to prevent double insert
		foreach (file; logfullname)
		{
			if (file.endsWith("zip"))
			{
				auto zip = new ZipArchive(file.read);
				foreach(ArchiveMember am; zip.directory)
				{
					// we are passing FulFileName to be able than detect if what kind of logs we are pasting (shgm3 or not)
					loginsert(file.replace("zip", "log"), cast(string)zip.expand(am)); //pass unpaked element of archive. Replace zip to log because it's already unpacked
				}
			}

			if(file.endsWith("log"))
			{
				// we are passing FulFileName to be able than detect if what kind of logs we are pasting (shgm3 or not)
				string txtfile = readText(file);
				loginsert(file, txtfile);
			}

		}
	}

	void loginsert(string logfullname, string fcontent) // Tables with needed fields are create automatically! See code above. 
	{
		try
		{
			writefln("[LOGs from FTP]");
			string log_original = logfullname.baseName; // save original name
			// we mast detect what kind of logs we are inserting shgm3 or no.
			// shgm3 logs are inserting in one table
			// adding case with "1" to prevent finding same pass
			if (logfullname.canFind(parseconfig.ftplocalpath)) // it's look like simple FTP LOGs
			{
				writeln("FTP standard logs");
				// we need split filename, to get from DESP-140101 ==> DESP AND from s1-DESP-140101 ==> S1-DESP
				// DO NOT CHANGE ORDER!!! OR WE WOULD CUT name FROM s1-DESP-140101 s1-DESP AND THEN CUT IT"S AGAIN!!!
				if (logfullname.baseName.countchars("-") == 1)
				{
					writeln("processing log: ",  logfullname);
					logfullname = logfullname.baseName.stripExtension.split("-")[0].replace("-","_"); //now we should give normal name for it. Ex: S1-ELISOVO-140511 -->  S1-ELISOVO
				}

				if (logfullname.baseName.countchars("-") == 2)
				{
					writeln("processing log: ",  logfullname);
					logfullname = logfullname.baseName.stripExtension.split("-")[0..2].join("-").replace("-","_"); //now we should give normal name for it. Ex: S1-ELISOVO-140511 -->  S1-ELISOVO
				}



				writeln("=>", logfullname);
				auto request_before = stmt.executeQuery("SELECT * FROM test.ftplogfiles WHERE name='" ~ log_original ~ "'");
				if(request_before.next() == false)
				{
					writefln("Do not in DB: %s", logfullname);		
					/////we need to check if table where we INSERT is exists!//////
					// this part if table DO NOT exists and we should create it!
					// FIXME: split should do splittings only after second "-", or tables may have different coloumns numbers
					auto dbshow = stmt.executeQuery("SHOW TABLES LIKE '" ~ logfullname ~ "';", );
				 	
				 	if(dbshow.next())
				 		cwritefln("Table: " ~ logfullname.color(fg.green) ~ " is exists");

				 	else
				 	{
					 	// Here we are checking numbers of colomns in text
					 	string[] lines = fcontent.splitLines();
					 	string head = lines[0].strip;
					 	string row = lines[0].split.map!(a=>format("`%s`, ", a)).join.chop.chop;
					 	int colnum = head.countchars(" "); // here we save number of columns

					 	// -1 REMOVED
					 	auto x = iota(0, colnum-1); // -1 to get actual number columns that we need
					 	string s_sequence_gen = to!string(x.map!(a => format("`S%s` VARCHAR(10) NULL DEFAULT NULL, ", a)).join).chop.chop ~ ");";	
						
					 	string create_d_t_sql = ("CREATE TABLE IF NOT EXISTS `" ~ logfullname ~ "` (`Date` DATE NULL DEFAULT NULL, `Time` TIME NULL DEFAULT NULL, ");
					 	auto rs = stmt.executeUpdate(create_d_t_sql ~ s_sequence_gen);
					 	// OK table with needed number of columns created

					}


				writeln("Processing standard insert operation...");
			 	foreach(line; fcontent.splitLines)
			 	{
			 		string row = line.split.map!(a=>format("'%s', ", a)).join.chop.chop; // constructor for S1 ... S(n)
			 		// ignore because it's rare situation when the data is already in the table
			 		if(parseconfig.ftp_load_only_1_minuts == "true")
			 		{
			 			string check_time = (line.split(" ")[1]).split(":")[2]; // getting last part of 12:14:_15_ now we check if it's one munute date. We need at to ton past all
			 			if(check_time != "00") continue;
			 		}

			 		string sql = "INSERT IGNORE INTO test." ~ logfullname ~ " VALUES(" ~ row ~ ");";
			 		//Thread.sleep( dur!("msecs")( 200 ) );
			 		auto rs = stmt.executeUpdate(sql);

			 	}

		 	
			 	// Now we should write that data was processed
			 	string sqlinsert = format("INSERT IGNORE INTO test.ftplogfiles (Name, Status) VALUES ('" ~ log_original ~ "', 'Done')");
				auto rs = stmt.executeUpdate(sqlinsert);
				core.thread.Thread.sleep( dur!("msecs")(300));	
			}

		 	else
		 		cwritefln("%s is already in DB", log_original.color(fg.red));
		}

		// we mast detect what kind of logs we are inserting shgm3 or no.
		// shgm3 logs are inserting in one table
		if (logfullname.canFind(parseconfig.ftplocalpath_shgm3)) // it's look like simple shgm3 LOGs
		{
			writeln("FTP shgm3");
			auto request_before = stmt.executeQuery("SELECT * FROM test.shgm3_stat WHERE name='" ~ log_original ~ "'");
			if(request_before.next() == false)
			{
				writeln(logfullname);
				writeln("Processing insert operation...");
				foreach(line; fcontent.splitLines)
				{
					string row = line.split.map!(a=>format("'%s', ", a)).join.chop.chop; // constructor for S1 ... S(n)
					// ignore because it's rare situation when the data is already in the table
					if(parseconfig.ftp_load_only_1_minuts == "true")
					{
						string check_time = (line.split(" ")[1]).split(":")[2]; // getting last part of 12:14:_15_ now we check if it's one munute date. We need at to ton past all
						if(check_time != "00") continue;
					}

					string sql = "INSERT IGNORE INTO test.shgm3 VALUES(" ~ row ~ ");";
					//writeln(sql);
				 	auto rs = stmt.executeUpdate(sql);
				}

				// Now we should write that data was processed
			 	string sqlinsert = format("INSERT IGNORE INTO test.shgm3_stat (Name, Status) VALUES ('" ~ log_original ~ "', 'Done')");
				auto rs = stmt.executeUpdate(sqlinsert);
				core.thread.Thread.sleep( dur!("msecs")(3000));	
			}
			
			else
			cwritefln("%s is already in DB", log_original.color(fg.red));


			}


	}

		catch(Exception e)
		{
			writeln("Error in loginsert");
			writeln(e.msg);
		}

	}


	void log_shgm3_insert(string logname, string fcontent)
	{
		/*
		try
		{	
			writefln("[LOGs SHGM3 from FTP]");
			Date logDate; // here we will store Date of latest log. Then get minus one day. And upload this file
			 // we need convert every log name to date. And compare it with current time. Coday log we may be get new data. 
			 // So we will upload it's multiple times (if it's changes -- the best variant).
			Date curdt = cast(Date)(Clock.currTime()); // name pattern is yymmdd (150101)
			string curdt_pattern = curdt.toISOString()[2..$]; // 20150210 --> 150210
			//writeln(curdt_pattern);
			foreach (i, logname; lognames)
			{
				writeln(logname);
				auto request_before = stmt.executeQuery("SELECT * FROM test.shgm3_stat WHERE name='" ~ logname ~ "'");
				if(request_before.next() == false)
				{
					writefln("Do not in DB: %s", logname);
					writefln(logfullname[i]);

					auto file = File(logfullname[i], "r");
					foreach (line; file.byLine)
	           		  {
	              		 char [][] colomns = line.split(" ");
	              		  string sqlinsert = format("INSERT INTO test.shgm3 (Date, Time, S1, S2, S3, S4) VALUES (" ~ "'"~ colomns[0] ~ "', '" ~ colomns[1] ~ "', " ~ "'"~ colomns[2] ~ "', " ~ "'"~ colomns[3] ~ "', '"~ colomns[4] ~ "', '"~ colomns[5] ~ "') " ~ "ON DUPLICATE KEY UPDATE Date = '" ~ colomns[0] ~ "', Time = '" ~ colomns[1] ~ "'");
	              		  //writefln(sqlinsert);
	               		  //stupid way to load only one one data string per minute
							if (parseconfig.ftp_load_only_1_minuts == "true")
							{
								if (colomns[1].split(":")[2].canFind("00")) //check time if seconds 00 than insert
								{
									auto rs = stmt.executeUpdate(sqlinsert);
								}
								else
									continue;
							}
							// if var is false we insert all date!
							else
							auto rs = stmt.executeUpdate(sqlinsert);
	               		  //FIX ME
	               		  // NEED UNIQ INDEX from two colomns!!!
	          		  }

	          		// trick! We will not put DONE status for current date, because file with date may be update!
	          		if (!logname.canFind(curdt_pattern)) // skip current date!
	          		{
	                	string sqlinsert = format("INSERT IGNORE INTO test.shgm3_stat (Name, Status) VALUES ('" ~ logname ~ "', 'Done')");
	                	//writefln(sqlinsert);
	                	auto rs = stmt.executeUpdate(sqlinsert);
	                }

				}
				else
					cwritefln("Already in DB: ", logname.color(fg.red));	
			}
		}
		
		catch(Exception e)	
		{
			writeln("Error in log_shgm3_insert");
			writeln(e.msg);
		}
		*/
	}

	void kakiokajma_insert(string name, string type, string status)
	{
		try
		{
			string sqlinsert = ("INSERT IGNORE INTO test.kakioka_plots (name, type, status) VALUES ('"~ name ~ "', '" ~ type ~ "' , '" ~ status ~"');");
			//writeln(sqlinsert);
			//writeln("---------------");
			stmt.executeUpdate(sqlinsert);
		}

		catch(Exception e)
		{
			writeln("Error in kakiokajma_insert section");
			writeln(e.msg);
		}

	}


}

// stand alone function
bool checkLink(string link) // we need to check all links to be sure if they are alive
{
	try
	{
		uint status;
		bool isalive;
		auto http = HTTP(link);
		http.onReceiveStatusLine = (HTTP.StatusLine status)
			{ 
				if ((status.code) == 200)
					{
						isalive = true;
						cwritefln("%s response code: %s".color(fg.green), link, (status.code)); 
					}	
				else
					{
						isalive = false;
						cwritefln("ERROR: %s response code: %s".color(fg.red), link, (status.code)); 
					}
		
			};
		http.onReceive = (ubyte[] data) { /+ drop +/ return data.length; };
		http.perform();

		return isalive;
	}
	
	catch(Exception ex)	
	{
		writeln(ex.msg);
		return false;
	}
}


class GetImgFullLinks
{
	
	ParseConfig parseconfig; // we need it's because basepath of IMG link are getting from config
	DBConnect db;

	string [] pagewithimgforparsing; // list of pages for extractring images from them
	string [] fullimgurl; // ALL links from NRL section. (I hope). Now only NRL section! 
	string [] imgnames;

	this (ParseConfig parseconfig, DBConnect db) // instanse of class Parse and GetImgLinks
	{
		this.parseconfig = parseconfig;
		this.db = db;

		writefln("[Exctacting images from pages]");
		writefln("[Checking pages]");
		foreach (url; parseconfig.pagewithimgforparsing)
			{
				// DO NOT FORGET UNCOMMENT!
				//  comment to prevent link check 
			
				if(checkLink(url))// we need to check if all links are alive and correct!
					this.pagewithimgforparsing ~=url;
				
				else
				{
					cwriteln("Next links in config.ini look like unreachable:".color(fg.red));
					writeln(url);
				}
				
				
			}

	}

	// here is another page where IMG storage. And we do not need to send post
	void nrlmrynavymil2()
	{
		try
		{
			writeln("Processing http://www.nrlmry.navy.mil/archdat/global/stitched/ (2 section)");

			string [] imgfullurls;
			string [] imgnames;

			string [] baseurls = ["http://www.nrlmry.navy.mil/archdat/global/stitched/ir/", 
								"http://www.nrlmry.navy.mil/archdat/global/stitched/vis/"];

			foreach(baseurl;baseurls)
			{
					if(!baseurl.endsWith("/")) 
				
				baseurl = baseurl ~ "/";

				string content = get(baseurl).idup;
				auto document = new dom.Document(content);

				//writeln(baseurl);
			    foreach(row; document.querySelectorAll(`a[href]`))
			    {
			    	if(!row.href.endsWith(".jpg")) continue; // skip not jpg files
			    	if(row.href.canFind("LATEST")) continue; // skip LATEST
			    	imgfullurls ~= baseurl ~ row.href;
			    	imgnames ~= row.href;
			    	//writefln(baseurl ~ row.href);
			    }
			 }
		
			db.IMGsInsert(imgfullurls, imgnames);
		}
		
		catch(Exception e)	
		{
			writeln("Error processing nrlmrynavymil2 --> http://www.nrlmry.navy.mil/archdat/global/stitched/ ");
			writeln(e.msg);
		}
	}	



	// on jma page we can't parse html to get all links (it's gen by js)
	// so we will use bruteforce
	void jmaimgs()
	{
		try
		{
			string [] imgurls; //all urls
			string [] aliveimgurls; //all urls
			string [] imgnames; //all urls
			//string basepart = "http://www.jma.go.jp/en/gms/imgs/5/infrared/0/";
			string basepart_fd = "http://www.jma.go.jp/en/gms/imgs/6/infrared/0/";
			string basepart_nh = "http://www.jma.go.jp/en/gms/imgs/5/infrared/0/";
			string basepart_nw = "http://www.jma.go.jp/en/gms/imgs/1/infrared/0/";
			string basepart_ne = "http://www.jma.go.jp/en/gms/imgs/3/infrared/0/";
			string basepart_sw = "http://www.jma.go.jp/en/gms/imgs/2/infrared/0/";
			string basepart_se = "http://www.jma.go.jp/en/gms/imgs/4/infrared/0/";
			string basepart_ea = "http://www.jma.go.jp/en/gms/imgs/0/infrared/0/";

			//string basepart = "http://www.jma.go.jp/en/gms/imgs/5/infrared/0/201502121615-00.png";
			DateTime dt = cast(DateTime)(Clock.currTime - 3.hours);
			string dtformat = dt.toISOString.replace("T", "")[0 .. 10]; //2015 02 12 16
															   			//2015 02 12 16 15-00.png
			//string fullurl = basepart ~ dtformat ~ "00-00.png";
			
			imgurls ~= basepart_fd ~ dtformat ~ "00-00.png"; 
			imgurls ~= basepart_nh ~ dtformat ~ "00-00.png";
			imgurls ~= basepart_nw ~ dtformat ~ "00-00.png";
			imgurls ~= basepart_ne ~ dtformat ~ "00-00.png";
			imgurls ~= basepart_sw ~ dtformat ~ "00-00.png";
			imgurls ~= basepart_se ~ dtformat ~ "00-00.png";
			imgurls ~= basepart_ea ~ dtformat ~ "00-00.png";
			writeln(imgurls.length);

			//ok all links in array, now we need to check them if they are alive
			foreach(i, url;imgurls)
			{
				if(checkLink(url))
				{
					aliveimgurls ~= url;
					imgnames ~= url[url.lastIndexOf("/")+1 .. $]; // extract name from url
				}
			}


			db.IMGsInsert(aliveimgurls, imgnames);
		}

		catch (Exception e)
		{
			writeln("Error in http://www.jma.go.jp");
			writeln(e.msg);
		}

	}


	// url hardcoded. becouse page gen by js
	void cwbimgs()
	{
		try
		{
			string [] imgurls; //all urls
			string [] aliveimgurls; //all urls
			string [] imgnames; //all urls

			string basepart_rgb = "http://www.cwb.gov.tw/V7/observe/satellite/Data/HSUP/"; // RGB
			string basepart_vis = "http://www.cwb.gov.tw/V7/observe/satellite/Data/HSXO/"; // VIS
			string basepart_enh = "http://www.cwb.gov.tw/V7/observe/satellite/Data/HS5Q/"; //Enhance

			//http://www.cwb.gov.tw/V7/observe/satellite/Data/HS5O/HS5O-2015-02-14-20-00.jpg
			DateTime dt = cast(DateTime)(Clock.currTime + 5.hours);
			//string dtformat = dt.toISOString.replace("T", "")[0 .. 10]; //2015021216

			// dirty hack!
			string dtformat = format(to!string(dt.year) ~ "-" ~ (to!string(to!int(dt.month))
																							.replace("1","01")) 
																							.replace("2","02")
																							.replace("3","03")
																							.replace("4","04")
																							.replace("5","05")
																							.replace("6","06")
																							.replace("7","07")
																							.replace("8","08")
																							.replace("9","09")

			~ "-" ~ to!string(dt.day) ~ "-" ~ (to!string(dt.hour))							.replace("1","01")
																							.replace("2","02")
																							.replace("3","03")
																							.replace("4","04")
																							.replace("5","05")
																							.replace("6","06")
																							.replace("7","07")
																							.replace("8","08")
																							.replace("9","09") );
			

			//string fullurl = basepart ~ dtformat ~ "00-00.png";
			
			imgurls ~= basepart_rgb ~"HSUP-" ~ dtformat ~ "-00.jpg"; 
			imgurls ~= basepart_rgb ~"HSUP-" ~ dtformat ~ "-30.jpg";

			imgurls ~= basepart_vis ~"HSXO-" ~ dtformat ~ "-00.jpg"; 
			imgurls ~= basepart_vis ~"HSXO-" ~ dtformat ~ "-30.jpg";

			imgurls ~= basepart_enh ~"HS5Q-" ~ dtformat ~ "-00.jpg"; 
			imgurls ~= basepart_enh ~"HS5Q-" ~ dtformat ~ "-30.jpg";

			writeln(imgurls.length);
			writefln(basepart_rgb);
			//ok all links in array, now we need to check them if they are alive
			foreach(i, url;imgurls)
			{
				if(checkLink(url))
				{
					aliveimgurls ~= url;
					imgnames ~= url[url.lastIndexOf("/")+1 .. $]; // extract name from url
				}
			}
			writeln(aliveimgurls.length);

			db.IMGsInsert(aliveimgurls, imgnames);
		}

		catch(Exception e)
		{
			writeln("Error in cwbimgs section");
			writeln(e.msg);
		}

	}

	// to prevent very big insert operation to DB we will skip latest 2 month
	void kalpana()
	{
		try
		{
			string baseurl = "http://202.54.31.45/archive/KALPANA-1/";
			// if parsed part is DO NOT start with "/" and END with "/"
			// it's right link

			// ?C=N;O=D <- do not need
			// ?C=M;O=A <- do not need
			// ?C=S;O=A <- do not need
			// ?C=D;O=A <- do not need
			// /archive/ <- do not need
			// ASIA-SECTOR/
			// FULL-DISK/
			// NORTHEAST-SECTOR/
			// NORTHWEST-SECTOR/
			// PRODUCTS/

			if(!baseurl.endsWith("/")) 
				baseurl = baseurl ~ "/";

			string content = get(baseurl).idup;
			auto document = new dom.Document(content);
			string [] urls;
			string currentyear = to!string((cast(DateTime)(Clock.currTime())).year); // we need to put to DB only current year

		    foreach(row; document.querySelectorAll(`a[href]`))
		    {
		    	if(row.href.canFind("PRODUCTS")) continue; // skip PRODUCTION folder
		    	if(!row.href.startsWith('/') && row.href.endsWith('/'))
		    	{
		    		string content1 = get(baseurl ~ row.href).idup;
		    		auto document1 = new dom.Document(content1);
		    		//writeln(content1);
					// here is 2 level folding

					foreach(row1; document1.querySelectorAll(`a[href]`))
				    {

				    	if(!row1.href.startsWith('/') && row1.href.endsWith('/'))
				    	{
			    			urls ~= baseurl ~ row.href ~ row1.href;
				    	}

				    }

		    	}
		    }
		    
		    DateTime currentdt = cast(Date)(Clock.currTime());
		    string lastyear = to!string(currentdt.roll!"years"(-1))[0..4];
		    foreach(url;urls)
		    {
		    	// it's better to past per folder, then collect all data. All data collection take too long time
		    	string [] fullurlimgs;
				string [] imgnames;
		    	writeln(url);
		    	string pagecontent = get(url).idup;
		    	auto page = new dom.Document(pagecontent);
		    	foreach (p; page.querySelectorAll(`a[href]`))
		    	{
		    		// we should skip name of base folders, like: /archive/KALPANA-1/ASIA-SECTOR/
		    		// idk why, but they are include in list of files
		    		if (p.href.endsWith("/"))	continue;
		    		if (!canFind(p.href, "jpg")) continue;
		    		if (canFind(p.href, lastyear)) continue;
		    			// to prevent to big date we will add to DB date for this year

		    		fullurlimgs ~= url ~ p.href;
		    		imgnames ~= p.href;
		    
		    	}
		    	db.IMGsInsert(fullurlimgs, imgnames);
		    	//writefln("%s files was parsed in this section", to!string(fullurlimgs.length));
		    }
		}

		catch(Exception e)
		{
			writeln("Error in kalpana section");
			writeln(e.msg);
		}	
		    
	}



}

class IMGDownload
{
	DBConnect db;
	ParseConfig parseconfig;
	string [] urls; // IMG that have NULL -- for downloading
	string [] names;

	// statistic
	int img_before; // we need to declarate it's here or var would not be viible
	int img_after; // we need to declarate it's here or var would not be visible
	int total; // COUNT OF ALL rows (with any status)
	
	this(DBConnect db, ParseConfig parseconfig)
	{
		writefln("[Image Downloading]");

		this.db = db;
		this.parseconfig = parseconfig;
	}


	// in this section we parse url2path.ini only!
	// storage paths in DB seems bad idea
	IniSection getpathfromconfig() // auto because it's Ini.Parse type
	{
		string url2path = buildPath((thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]), "url2path.ini");
		if (!exists(url2path)) throw new Exception("ERROR: url2path.ini do not exists");
		auto url2pathconfig = Ini.Parse(url2path);
		try
		{

			foreach (i, value; url2pathconfig.keys)
			{
				//writeln(i);
				if (!exists(value))
				{
					cwritefln("%s DO NOT exists".color(fg.red), value);
					writeln("Please fix path in url2path.ini before continue!");
					core.thread.Thread.sleep( dur!("seconds")(10));
				}
				//if(to!int(i) == 0)
				//	writeln("Look like all string in url2path.ini commented or absent");

			}

		}

		catch(Exception e)
		{
			writeln("Error in getpathfromconfig");
			writeln(e.msg);
		}

		return url2pathconfig;

	}




void imagedownload(string sql) // standalone function for loading images 2FS
{
	try
	{
		auto total_request = db.stmt.executeQuery("select COUNT(*) from test.imgs;");
		while(total_request.next())
		total = to!int(total_request.getString(1));

		auto request_before = db.stmt.executeQuery("select COUNT(*) from test.imgs WHERE status='DONE';");
		while(request_before.next())
		img_before = to!int(request_before.getString(1));
	// see bottom for after request


		auto newimages = db.stmt.executeQuery(sql); // Request for NO Value in field

		while(newimages.next())
		{
			string url = newimages.getString(1);
			string name = newimages.getString(2);

			urls ~= url; // array of images that have NULL for downloading
			names ~= name;

			//writeln(url);
		}
		
	}

	catch(Exception ex)
	{
		writeln(ex.msg);
		writeln("Can't get IMGs with status NULL");
	}

	// before downloding NRL we should send post request!


	foreach(i, url; urls)
	{
		try 
		{
				int n=0; // check if at last one equals are present
				foreach (key, value; getpathfromconfig.keys) //cheking ALL keys! Of one of may is equal OK!
				{
					//writeln(key); //this way we accessing key of associative array string[string]
					//now we try to cpmpare url from config and (full)url in DB
					// if full url include url in config then getting path to download
					// key = path from config
					// if key from config = url, then getting vkey-value from config (path)
					// !key is not key name, but key is value!
					// compare URL From DB and URL from config!
					// very good idea. It's allow to change download path in any time!
					// it's seems that key here is element of structure. Can't remember!
					// example of key: http://www.jma.go.jp/en/gms/imgs/3/infrared/0/

					if (url.canFind(key))
						{
							//writeln("value from config: " ~ key); // just url base part
							//writeln("value from DB: " ~ url); // http:// ... .jpg
							//writeln();
							if (!(value.endsWith("\\"))) // if forget slashes in config building path will be failed
								{
									writeln(value);
									writeln();
									value = value ~ "\\";
								}
							//writeln(buildPath(value));
							// value is key-value (place for downloading on FS)
							download(url, value ~ names[i]);
							writefln("Downloading: %s\n", url);
						}
					n++;
				}
				if (n<1) cwriteln("Can't understand where to download URL url2path.ini empty?".color(fg.red));
			
			//if success we should update status
			string sqlupdate = ("UPDATE test.imgs SET status='DONE' WHERE src='" ~ url ~ "'");
			auto rs = db.stmt.executeUpdate(sqlupdate);


		}

		catch (Exception e)
		{
			writeln();
			cwriteln(e.msg.color(fg.red));
			cwriteln("Check if destination folder is exists".color(fg.red));
			writeln();
			string sqlupdate = ("UPDATE test.imgs SET status='FAIL' WHERE src='" ~ url ~ "'");
			auto rs = db.stmt.executeUpdate(sqlupdate);

		}

	}


		//Now calculate quantity after. To get statistic how many imgs was processed

		auto request_after = db.stmt.executeQuery("select COUNT(*) from test.imgs WHERE status='DONE';");
		while(request_after.next())
		img_after = to!int(request_after.getString(1));


	//statistic
	auto delta = img_after - img_before;
	//We include in statistic only string that marked as DONE
	cwritefln("Total Images in DB: %s | With DONE status: %s | New added: %s".color(fg.yellow), total, img_before, delta);
	writefln("No guaranty that all DONE images will storage on FS forever, but if they was DONE they was loaded successfully");
}

}

class SolarIndex
{
	ParseConfig parseconfig;
	DBConnect db;

	this(ParseConfig parseconfig, DBConnect db)
	{
		writefln("[SolarIndex]");
		this.parseconfig = parseconfig;
		this.db = db;
	}

	// we do not need collect commented dates...
	string [] date;
/*	string [] ghz;
	string [] magnetic_2K;
	string [] magnetic_1K;
	string [] noaa;
	string [] star;
	string [] potsdam;
	string [] daily;
	string [] planetary;
*/
	string [] boulder;

/*	string [] solarwind; */

//	string [] number_c;
//	string [] number_m;
//	string [] number_x;
	string [] sqls; // data for inserting



	void parse()
	{
		try
		{
			if(checkLink(parseconfig.solarindex)) 
				string content = get(parseconfig.solarindex).idup;
		}

		catch(Exception e)
		{
			writefln("Can't get content from %s", parseconfig.solarindex);
		}

	try
	{
		string content = get(parseconfig.solarindex).idup;

		auto document = new dom.Document(content);

	    foreach(row; document.querySelectorAll("tr"))
	    {
	    	auto data = row.querySelectorAll("td");
	    	if(data.length == 0)
	    		continue;
	    	//writeln(data[0].innerText, " ", data[1].innerText, " ", data[2].innerText);
	    
	    	date ~= to!string(data[0].innerText);
	/*		ghz ~= to!string(data[1].innerText);
			magnetic_2K ~= to!string(data[2].innerText);
			magnetic_1K ~= to!string(data[3].innerText);
			noaa ~= to!string(data[4].innerText);
			star ~= to!string(data[5].innerText);
			potsdam ~= to!string(data[6].innerText);
			daily ~= to!string(data[7].innerText);
			planetary ~= to!string(data[8].innerText); */

			boulder ~= (to!string(data[9].innerText)).map!(a => format("%s ", a)).join;
			//auto boulder = boulder.map!(a => format("%s ", a)).join;

	/*		solarwind ~= to!string(data[10].innerText); */
			// temporary commented till find way to replace &nbsp; 		
			//number_c ~= to!string(data[11].innerText);
			//number_m ~= to!string(data[12].innerText);
			//number_x ~= to!string(data[13].innerText);

	    }
	    
	    // we need to do reverse order, because other table table habe another date order
	    foreach_reverse (i, d; date)
	    {
	    	// full old version // string sql = format("INSERT INTO test.solarindex (date, ghz, magnetic_2K, magnetic_1K, noaa, star, potsdam, daily, planetary, boulder, solarwind) VALUES (%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE date=%s;",  d, ghz[i], magnetic_2K[i], magnetic_1K[i], noaa[i], star[i], potsdam[i], daily[i], planetary[i], boulder[i], solarwind[i], d);
			string sql = format("INSERT INTO test.dgd (date, boulder) VALUES ('%s', '%s') ON DUPLICATE KEY UPDATE boulder='%s';",  d, boulder[i], boulder[i]);	    	
	    	//writeln(sql);
	    	sqls ~= sql;
	    }

	    // befor sending it's to insert we need to create array of strings
	    db.solarindexinsert(sqls);

	}

	catch(Exception e)
	{
		writefln("Error in parsing section!");
	}


	}

}

class FTP
{
	ParseConfig parseconfig;
	DBConnect db;
	
	this(ParseConfig parseconfig, DBConnect db)
	{
		this.parseconfig = parseconfig;
		this.db = db;
	}

	string [] lognames;
	string [] logfullname;

	void getLogsList(string path)
	{
		try
		{
			if (!exists(path))
			{
				cwriteln("\nFTP local path do not exists".color(fg.red));
				writeln("Please use tool ftpuse and mount FTP to local folder");
				core.thread.Thread.sleep(dur!("seconds")(10));
				return;
			}

			if (!exists(path))
			{
				cwriteln("\nFTP local path (shgm3) do not exists".color(fg.red));
				writeln("Please use tool ftpuse and mount FTP to local folder");
				core.thread.Thread.sleep(dur!("seconds")(10));
				return;
			}

			auto logfiles = dirEntries(path, "*.{log,zip}", SpanMode.breadth);
			foreach(name; logfiles)
			{
				lognames ~= baseName(name);
				logfullname ~= name;
				//writeln(name);
			}

			db.getFTPLogContent(logfullname); // sending array of names to db instance


		}

		catch (Exception e)
		{
			writeln("Error in getLogsList section");
			writeln(e.msg);
		}

	}


}

class Kakiokajma
{
	DBConnect db;
	
	this(DBConnect db)
	{
		this.db = db;
	}

	void kakiokajma()
	{

		try
		{
			writeln("Loading PLOTS from kakiokajma");
			string confpath = buildPath((thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]), "url2path.ini");
			auto config = Ini.Parse(confpath);
				
			string geomagnetic_kak = config.getKey("geomagnetic_kak");
			string electric_kak = config.getKey("electric_kak");

			string geomagnetic_mmb = config.getKey("geomagnetic_mmb");
			string electric_mmb = config.getKey("electric_mmb");

			string geomagnetic_kny = config.getKey("geomagnetic_kny");
			string electric_kny = config.getKey("electric_kny");

			string geomagnetic_cbi = config.getKey("geomagnetic_cbi");
			string electric_cbi = config.getKey("electric_cbi");


			Date currentdt = cast(Date)(Clock.currTime());
			currentdt = currentdt.roll!"days"(-1);
			string year = to!string(currentdt.year);
			string month = to!string(currentdt.month);
			string day = to!string(currentdt.day);	

			string baseurl = "http://www.kakioka-jma.go.jp/cgi-bin/plot/plotNN.pl";

			string geomagnetic_kak_post = "place=kak&lang=en&datatype=provisional&datakind=m&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";
			string electric_kak_post = "place=kak&lang=en&datatype=provisional&datakind=e&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";

			string geomagnetic_mmb_post = "place=mmb&lang=en&datatype=provisional&datakind=m&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";
			string electric_mmb_post = "place=mmb&lang=en&datatype=provisional&datakind=e&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";

			string geomagnetic_kny_post = "place=kny&lang=en&datatype=provisional&datakind=m&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";
			string electric_kny_post = "place=kny&lang=en&datatype=provisional&datakind=e&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";

			string geomagnetic_cbi_post = "place=cbi&lang=en&datatype=provisional&datakind=m&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";
			string electric_cbi_post = "place=cbi&lang=en&datatype=provisional&datakind=e&sampling=1-min&hipasssec=150&year=" ~ year ~ "&month=" ~ month ~ "&day=" ~ day ~ "&hour=0&min=0";


		    string content_geomagnetic_kak = post(baseurl, geomagnetic_kak_post).idup;
		    string content_electric_kak = post(baseurl, electric_kak_post).idup;
		    string content_geomagnetic_mmb = post(baseurl, geomagnetic_mmb_post).idup;
		    string content_electric_mmb = post(baseurl, electric_mmb_post).idup;
		    string content_geomagnetic_kny = post(baseurl, geomagnetic_kny_post).idup;
		    string content_electric_kny = post(baseurl, electric_kny_post).idup;
		    string content_geomagnetic_cbi = post(baseurl, geomagnetic_cbi_post).idup;
		    string content_electric_cbi = post(baseurl, electric_cbi_post).idup;


		    auto pngRegex = regex(r"(/.+png)");


		    auto imgpng = matchFirst(content_geomagnetic_kak, pngRegex)[0]; // to prevent 2 elemets 

			string imgpng_geomagnetic_kak = matchFirst(content_geomagnetic_kak, pngRegex)[0];
			string imgpng_electric_kak = matchFirst(content_electric_kak, pngRegex)[0];
			string imgpng_geomagnetic_mmb = matchFirst(content_geomagnetic_mmb, pngRegex)[0];
			string imgpng_electric_mmb = matchFirst(content_electric_mmb, pngRegex)[0];
			string imgpng_geomagnetic_kny = matchFirst(content_geomagnetic_kny, pngRegex)[0];
			string imgpng_electric_kny = matchFirst(content_electric_kny, pngRegex)[0];
			string imgpng_geomagnetic_cbi = matchFirst(content_geomagnetic_cbi, pngRegex)[0];
			string imgpng_electric_cbi = matchFirst(content_electric_cbi, pngRegex)[0];

		    string fullImgURL_geomagnetic_kak = "http://www.kakioka-jma.go.jp" ~ imgpng_geomagnetic_kak;
		    string fullImgURL_electric_kak = "http://www.kakioka-jma.go.jp" ~ imgpng_electric_kak;
		    string fullImgURL_geomagnetic_mmb = "http://www.kakioka-jma.go.jp" ~ imgpng_geomagnetic_mmb;
		    string fullImgURL_electric_mmb = "http://www.kakioka-jma.go.jp" ~ imgpng_electric_mmb;
		    string fullImgURL_geomagnetic_kny = "http://www.kakioka-jma.go.jp" ~ imgpng_geomagnetic_kny;
		    string fullImgURL_electric_kny = "http://www.kakioka-jma.go.jp" ~ imgpng_electric_kny;
		    string fullImgURL_geomagnetic_cbi = "http://www.kakioka-jma.go.jp" ~ imgpng_geomagnetic_cbi;
		    string fullImgURL_electric_cbi = "http://www.kakioka-jma.go.jp" ~ imgpng_electric_cbi;
		    
		    try
		    {
				download(fullImgURL_geomagnetic_kak, geomagnetic_kak ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "geomagnetic_kak", "DONE");
			}

			catch(Exception e)
			{
				writeln("Can't load geomagnetic_kak");
			}

			try
			{
				download(fullImgURL_electric_kak, electric_kak ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "electric_kak", "DONE");
			}

			catch(Exception e)
			{
				writeln("Can't load electric_kak");
			}

			try
			{
				download(fullImgURL_geomagnetic_mmb, geomagnetic_mmb ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "geomagnetic_mmb", "DONE");
			}

			catch(Exception e)
			{
				writeln("Can't load geomagnetic_mmb");
			}

			try
			{
				download(fullImgURL_electric_mmb, electric_mmb ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "electric_mmb", "DONE");
			}

			catch(Exception e)
			{
				writeln("Can't load electric_mmb");
			}
			
			try
			{
				download(fullImgURL_geomagnetic_kny, geomagnetic_kny ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "geomagnetic_kny", "DONE");	
			}

			catch(Exception e)
			{
				writeln("Can't load geomagnetic_kny");
			}

			try
			{
				download(fullImgURL_electric_kny, electric_kny ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "electric_kny", "DONE");	
			}

			catch(Exception e)
			{
				writeln("Can't load electric_kny");
			}

			try
			{
				download(fullImgURL_geomagnetic_cbi, geomagnetic_cbi ~ to!string(currentdt.toISOString) ~ ".png");
				db.kakiokajma_insert(currentdt.toISOString, "geomagnetic_cbi", "DONE");	
			}

			catch(Exception e)
			{
				writeln("Can't load geomagnetic_cbi");
			}
			
			try
			{
				download(fullImgURL_electric_cbi, electric_cbi ~ to!string(currentdt.toISOString) ~ ".png");	
				db.kakiokajma_insert(currentdt.toISOString, "electric_cbi", "DONE");	
			}

			catch(Exception e)
			{
				writeln("Can't load electric_cbi");
			}
			
		}

		catch(Exception e)
		{
			writeln(e.msg);
			writeln("Try to turn off section kakiokajma in config.ini");
		}

	}

}


class ObsmpPlots
{
	DBConnect db;
	
	this(DBConnect db)
	{
		this.db = db;
	}

	void getPlots()
	{
		try
		{	
			writeln("Loading ObsmpPlots");
			string confpath = buildPath((thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]), "url2path.ini");
			auto config = Ini.Parse(confpath);

			string currentdt = to!string((cast(Date)(Clock.currTime())).toISOString);				

			string dt_1 = to!string((cast(Date)(Clock.currTime()) + 1.days).toISOString);
			string dt_14 = to!string((cast(Date)(Clock.currTime()) + 14.days).toISOString);	
			string dt_30 = to!string((cast(Date)(Clock.currTime()) + 30.days).toISOString);	

			string var_x_1        = config.getKey("var_x_1");
			string var_x_14       = config.getKey("var_x_14");
			string var_x_30       = config.getKey("var_x_30");

			string var_y_1        = config.getKey("var_y_1");
			string var_y_14       = config.getKey("var_y_14");
			string var_y_30       = config.getKey("var_y_30");

			string var_pol_1      = config.getKey("var_pol_1");
			string var_pol_14     = config.getKey("var_pol_14");
			string var_pol_30     = config.getKey("var_pol_30");

			string var_ut1utc_1   = config.getKey("var_ut1utc_1");
			string var_ut1utc_14  = config.getKey("var_ut1utc_14");
			string var_ut1utc_30  = config.getKey("var_ut1utc_30");

			string var_ut1tai_1   = config.getKey("var_ut1tai_1");
			string var_ut1tai_14  = config.getKey("var_ut1tai_14");
			string var_ut1tai_30  = config.getKey("var_ut1tai_30");

			string var_lod_1      = config.getKey("var_lod_1");
			string var_lod_14     = config.getKey("var_lod_14");
			string var_lod_30     = config.getKey("var_lod_30");

		
			//download(fullImgURL_electric_cbi, electric_cbi ~ to!string(currentdt.toISOString) ~ ".png");	
			//db.kakiokajma_insert(currentdt.toISOString, "electric_cbi", "DONE");

			string url_var_x_1 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=1&dimx=600&dimy=350&tver=0";
			string url_var_x_14 ="http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=14&eop=1&graphe=1&dimx=600&dimy=350&tver=0";
			string url_var_x_30 ="http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=30&eop=1&graphe=1&dimx=600&dimy=350&tver=0";

			string url_var_y_1 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=2&dimx=600&dimy=350&tver=0";
			string url_var_y_14 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=14&eop=1&graphe=2&dimx=600&dimy=350&tver=0";
			string url_var_y_30 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=30&eop=1&graphe=2&dimx=600&dimy=350&tver=0";

			string url_var_pol_1 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=12&dimx=600&dimy=600&tver=0";
			string url_var_pol_14 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=14&eop=1&graphe=12&dimx=600&dimy=600&tver=0";
			string url_var_pol_30 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=30&eop=1&graphe=12&dimx=600&dimy=600&tver=0";

			string url_var_ut1utc_1 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=3&dimx=600&dimy=350&tver=0";
			string url_var_ut1utc_14 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=14&graphe=3&dimx=600&dimy=350&tver=0";
			string url_var_ut1utc_30 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=30&graphe=3&dimx=600&dimy=350&tver=0";

			string url_var_ut1tai_1 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=4&dimx=600&dimy=350&tver=0";
			string url_var_ut1tai_14 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=14&eop=1&graphe=4&dimx=600&dimy=350&tver=0";
			string url_var_ut1tai_30 = "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=30&eop=1&graphe=4&dimx=600&dimy=350&tver=0";

			string url_var_lod_1 =  "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=1&eop=1&graphe=5&dimx=600&dimy=350&tver=0";		
			string url_var_lod_14 =  "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=14&eop=1&graphe=5&dimx=600&dimy=350&tver=0";		
			string url_var_lod_30 =  "http://hpiers.obspm.fr/eop-pc/products/combined/realtime/realtimeplot.php?laps=30&eop=1&graphe=5&dimx=600&dimy=350&tver=0";		


			download(url_var_x_1, var_x_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_x_1" ~ "');");

			download(url_var_x_14, var_x_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_x_14" ~ "');");

			download(url_var_x_30, var_x_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_x_30" ~ "');");

			////
			download(url_var_y_1, var_y_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_y_1" ~ "');");

			download(url_var_y_14, var_y_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_y_14" ~ "');");

			download(url_var_y_30, var_y_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_y_30" ~ "');");			

			//////
			download(url_var_pol_1, var_pol_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_pol_1" ~ "');");

			download(url_var_pol_14, var_pol_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_pol_14" ~ "');");

			download(url_var_pol_30, var_pol_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_pol_30" ~ "');");			

			////
			download(url_var_ut1utc_1, var_ut1utc_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_ut1utc_1" ~ "');");

			download(url_var_ut1utc_14, var_ut1utc_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_ut1utc_14" ~ "');");

			download(url_var_ut1utc_30, var_ut1utc_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_ut1utc_30" ~ "');");			

			//////
			download(url_var_ut1tai_1, var_ut1tai_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_ut1tai_1" ~ "');");

			download(url_var_ut1tai_14, var_ut1tai_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_ut1tai_14" ~ "');");

			download(url_var_ut1tai_30, var_ut1tai_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_ut1tai_30" ~ "');");			

			/////

			download(url_var_lod_1, var_lod_1 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_lod_1" ~ "');");

			download(url_var_lod_14, var_lod_14 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_14 ~"', '" ~ dt_14 ~ ".png', '" ~ "var_lod_14" ~ "');");

			download(url_var_lod_30, var_lod_30 ~ currentdt ~ ".png");
			db.stmt.executeUpdate("INSERT INTO test.obsmpplots (DateStart, DateEnd, Name, Type) VALUES ('" ~ currentdt ~ "', '" ~ dt_30 ~"', '" ~ dt_30 ~ ".png', '" ~ "var_lod_30" ~ "');");			
			
		}

	catch(Exception e)
	{
		writeln("Error in ObsmpPlots (getPlots)");
		writeln(e.msg);
	}

	}
}


class DGDIndex 
{
	ParseConfig parseconfig;
	DBConnect db;

	this(ParseConfig parseconfig,  DBConnect db)
	{
		this.parseconfig = parseconfig;
		this.db = db;
	}

	string [] date;
	string [] fredericksburg;
	string [] college;
	string [] planetary;

	string [] sqls; // data for inserting


	void parse()
	{
		try
		{

		//	string url = "ftp://ftp.swpc.noaa.gov/pub/indices/DGD.txt";

			foreach(line; parseconfig.dgd.byLine())
			{
				if (to!string((line[0])) != "2") continue; // stupid way to check header
				//writeln(line);
				//readln();
				date ~= (to!string(line)).replace(" ", "-")[0..10];
				
				fredericksburg ~= ((to!string(line)).replace("-", " ")[18..34]).strip;
				college ~= ((to!string(line)).replace("-", " ")[40..57]).strip;
				planetary ~= ((to!string(line)).replace("-", " ")[62..79]).strip;		

			}

			foreach (i, date; date)
			{
				string sqlinsert = format("INSERT INTO test.DGD (`date`, `fredericksburg`, `college`, `planetary`) VALUES ('%s', '%s', '%s', '%s') ", date, fredericksburg[i], college[i], planetary[i]);
				//writeln(sqlinsert);
				string onDuplicateKey = format(" ON DUPLICATE KEY UPDATE fredericksburg='%s', college='%s', planetary='%s';", fredericksburg[i], college[i], planetary[i]);
				//writeln(onDuplicateKey);
				string sqlresult = sqlinsert ~ onDuplicateKey;
				sqls ~= sqlresult;
			}
		    db.dgdindexinsert(sqls);

		}

		catch(Exception e)
		{
			writefln("Error in parsing section DGDIndex!");
		}


	}

}

class Nadisa
{
	DBConnect db;
	
	this(DBConnect db)
	{
		this.db = db;
	}

	void getPlots()
	{
		try
		{
			// http://nadisa.org/lab2/3-150118-150201.jpg
			string baseurl = "http://nadisa.org/lab2/3-";
			writeln("Loading Nadisa");
			string confpath = buildPath((thisExePath[0..((thisExePath.lastIndexOf("\\"))+1)]), "url2path.ini");
			auto config = Ini.Parse(confpath);

			string currentdt = to!string((cast(Date)(Clock.currTime())).toISOString);				

			string dt_14 = to!string((cast(Date)(Clock.currTime()) - 14.days).toISOString)[2..$];	
			string dt_30 = to!string((cast(Date)(Clock.currTime()) - 30.days).toISOString)[2..$];	

			string url_14 = baseurl ~ dt_14 ~ "-" ~ currentdt[2..$] ~ ".jpg";
			string url_30 = baseurl ~ dt_30 ~ "-" ~ currentdt[2..$] ~ ".jpg";

			string nadisa_14  = config.getKey("nadisa_14");
			string nadisa_30  = config.getKey("nadisa_30");

			//writeln(currentdt[2..$]);

			//writeln(url_30);

			download(url_14, nadisa_14 ~ currentdt ~ ".jpg");
			db.stmt.executeUpdate("INSERT IGNORE INTO test.Nadisa_plots (name, date_start, date_end, status) VALUES ('" ~ currentdt ~ ".jpg', " ~ dt_14 ~ " ," ~ currentdt ~ ", 'DONE')");
			//db.stmt.executeUpdate("INSERT IGNORE INTO test.Nadisa (name, status) VALUES ('" ~ currentdt ~ "', '" ~ dt_1 ~"', '" ~ dt_1 ~ ".png', '" ~ "var_x_1" ~ "');");

			download(url_30, nadisa_30 ~ currentdt ~ ".jpg");
			db.stmt.executeUpdate("INSERT IGNORE INTO test.Nadisa_plots (name, date_start, date_end, status) VALUES ('" ~ currentdt ~ ".jpg', " ~ dt_30 ~ " ," ~ currentdt ~ ", 'DONE')");
		}

		catch(Exception e)
		{
			writeln("Error in Nadisa section (getPlots)");
			writeln(e.msg);
		}

	}


}